* Configuration
Configuration for tux51, tux52 and tux54 is like the previous class.

** Switch
Connected to:
- tux51 E0 :: Port Fa0/1 \to vlan50
- tux52 E0 :: Port Fa0/2 \to vlan51
- tux54 E0 :: Port Fa0/4 \to vlan50
- tux54 E1 :: Port Fa0/3 \to vlan51
- Router   :: Port Gi0/1 \to vlan51

#+begin_src sh
enable
# Password...
configure terminal

interface Gi0/1
switchport mode access
switchport access vlan 51

^Z
#+end_src

** Router
Connected to:
- Switch   :: Port Gi0/0 \to vlan51
- Internet :: Port Gi0/1

#+begin_src sh
# Password...
configure terminal

interface Gi0/0
ip address 172.16.51.254 255.255.255.0
no shutdown

interface Gi0/1
ip address 172.16.1.59 255.255.255.0
no shutdown

ip route 0.0.0.0     0.0.0.0       172.16.1.254
ip route 172.16.50.0 255.255.255.0 172.16.51.253

^Z

# In order to list interface information
show interface Gi0/0
#+end_src

*** Save configuration
#+begin_src sh
copy running-config flash:team_goncalo_joao_pedro
#+end_src

*** Reload configuration
#+begin_src sh
# copy flash:tux5-clean startup-config
copy flash:team_goncalo_joao_pedro startup-config
reload
#+end_src

** Machines configuration
*** tux51 configuration
#+begin_src sh
ip route add default via 172.16.50.254
#+end_src

*** tux52 & tux54
#+begin_src sh
ip route add default via 172.16.51.254
#+end_src

** Ping redirect
#+begin_src sh
echo 0 > /proc/sys/net/ipv4/conf/eth0/accept_redirects
echo 0 > /proc/sys/net/ipv4/conf/all/accept_redirects

ip route del 172.16.50.0/24
#+end_src

** Router with NAT
#+begin_src sh
# Password...
configure terminal

interface Gi0/0
ip nat inside

interface Gi0/1
ip nat outside

exit

ip nat pool ovrld 172.16.1.59 172.16.1.59 prefix 24
ip nat inside source list 1 pool ovrld overload

access-list 1 permit 172.16.50.0 0.0.0.7
access-list 1 permit 172.16.51.0 0.0.0.7
#+end_src

Only IP addresses with a mask of 0.0.0.7 are allowed to pass the NAT, from the inside to the outside.

*** Ping from tux51
Everything worked great, as tux51 has an IP address of 172.16.50.1

*** Ping from tux54
This didn't work, because the IP address is 172.16.51.253, so it is not allowed to pass the NAT.

This also means that, if tux54 had NAT itself, the NICs from vlan50 (which would be converted to
172.16.50.253) couldn't access the outside.
