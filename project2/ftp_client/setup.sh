set -euo pipefail
RootDir="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

SrcDir="$RootDir/src"
DepsDir="$RootDir/deps"
BuildDir="$RootDir/build"

mkdir -p "$BuildDir"

DebugFlags="-ggdb"
ReleaseFlags="-O2"
Flags="$DebugFlags"

Flags="$Flags -I$DepsDir/glew/include -I$DepsDir/glfw/include -I$DepsDir/imgui"
DepsFlags="$Flags"

Flags="$Flags -Wall -Wextra -Wno-unused-function -Wno-write-strings -Wno-missing-field-initializers"

Flags="$Flags -ldl -lX11 -pthread -lGL"

DepsBuildDir="$BuildDir/deps"
Flags="$Flags $DepsBuildDir/*"
mkdir -p "$DepsBuildDir"
