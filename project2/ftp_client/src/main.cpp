#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl2.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <poll.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define arrayCount(Arr) sizeof(Arr)/sizeof(*(Arr))

#define invalidCodePath() assert(!"Invalid code path")
#define invalidCase(Case) case Case: { assert(!"Invalid case"); } break
#define invalidDefaultCase() default: { assert(!"Invalid default case"); } break

#define staticAssert(X) typedef int __static_assert_##__FILE__##__LINE__[(X) ? 1 : -1]

typedef int8_t  s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

#include "stretchy.hpp"

enum {
    ProgressFrames = 30,
    TimeoutFrames = 600,
    MaxSentLine = 2048,
    FileChunkSizes = 4096,
};

typedef enum {
    FtpCode_RestartMarker        = 110,
    FtpCode_ServiceAlmostRdy     = 120,
    FtpCode_TransferStarting     = 125,
    FtpCode_OpeningData          = 150,

    FtpCode_Okay                 = 200,
    FtpCode_Superfluous          = 202,
    FtpCode_SysStatus            = 211,
    FtpCode_DirStatus            = 212,
    FtpCode_FileStatus           = 213,
    FtpCode_HelpMsg              = 214,
    FtpCode_SysType              = 215,
    FtpCode_ReadyForUser         = 220,
    FtpCode_ControlClosed        = 221,
    FtpCode_DataOpen             = 225,
    FtpCode_DataClosed           = 226,
    FtpCode_PassiveMode          = 227,
    FtpCode_LoggedIn             = 230,
    FtpCode_FileActionCompleted  = 250,
    FtpCode_PathCreated          = 257,

    FtpCode_NeedPassword         = 331,
    FtpCode_NeedUser             = 332,
    FtpCode_FileActionPending    = 350,

    FtpCode_ServiceNotAvailable  = 421,
    FtpCode_DataOpeningError     = 425,
    FtpCode_ConnectionAborted    = 426,
    FtpCode_FileBusy             = 450,
    FtpCode_ActionAborted        = 451,
    FtpCode_InsufficientStorage  = 452,

    FtpCode_SyntaxError          = 500,
    FtpCode_SyntaxErrorArgs      = 501,
    FtpCode_NotImplemented       = 502,
    FtpCode_BadSequence          = 503,
    FtpCode_InvalidParameter     = 504,
    FtpCode_NotLoggedIn          = 530,
    FtpCode_NeedUserForStoring   = 532,
    FtpCode_FileUnavailable      = 550,
    FtpCode_PageTypeUnknown      = 551,
    FtpCode_StorageAllocExceeded = 552,
    FtpCode_InvalidFileName      = 553,
} ftp_code;

typedef enum {
    Ftp_Disconnected,
    Ftp_Loading,
    Ftp_Connected,
} ftp_conn_state;

typedef enum {
    FtpCmd_Noop,

    FtpCmd_User,
    FtpCmd_Password,
    FtpCmd_Type,
    FtpCmd_Passive,
    FtpCmd_DirList,
    FtpCmd_DirListAlt,
    FtpCmd_ChangeDir,
    FtpCmd_Retrieve,
    FtpCmd_Quit,
} ftp_command;

typedef struct {
    char *Name;
    bool Dir;
} dir_item;

typedef struct {
    int FramesWaiting;
    int CtrlSocket;
    u32 CtrlRxLength;
    u8  CtrlRx[1<<8];
    int DataSocket;
    u32 DataLength;
    u32 DataCap;
    u8 *DataRx;
    ftp_conn_state State;
    ftp_command LastCommand;
    ftp_command DataCommand;
    ftp_command CommandWaitingForData;
    bool HasDir;
    bool UseAltDirListing;
    dir_item *DirList;
    char User[128];
    char *FileToRetrieve;
    int DestFile;
    char Password[128];
    char ErrorMsg[500];
} ftp_conn;

static inline bool ftpIsWaiting(ftp_conn *Conn) {
    return Conn->LastCommand || Conn->DataCommand || Conn->CommandWaitingForData;
}

static void resetFtpConn(ftp_conn *Conn) {
    if(Conn->CtrlSocket >= 0) {
        close(Conn->CtrlSocket);
    }

    if(Conn->DataSocket >= 0) {
        close(Conn->DataSocket);
    }

    if(Conn->DirList) {
        for(int Idx = 0; Idx < bufLength(Conn->DirList); ++Idx) {
            free(Conn->DirList[Idx].Name);
        }
        bufFree(Conn->DirList);
    }
    Conn->HasDir = false;

    Conn->CtrlSocket = -1;
    Conn->DataSocket = -1;
    Conn->State = Ftp_Disconnected;
    Conn->LastCommand  = FtpCmd_Noop;
    Conn->CommandWaitingForData  = FtpCmd_Noop;
    Conn->CtrlRxLength  = 0;
    Conn->FramesWaiting  = 0;
    Conn->UseAltDirListing = false;
    Conn->DestFile = -1;

    if(Conn->DataRx) {
        free(Conn->DataRx);
        Conn->DataRx = 0;
    }
    Conn->DataLength = Conn->DataCap = 0;
}

static void glfwErrorCallback(int Error, const char *Description) {
    fprintf(stderr, "Glfw Error %d: %s\n", Error, Description);
}

static inline void writeErrorMsg(ftp_conn *Conn, char *Format, ...) {
    va_list Args;
    va_start(Args, Format);
    vsnprintf(Conn->ErrorMsg, arrayCount(Conn->ErrorMsg), Format, Args);
    va_end(Args);
}

static inline void sendUser(ftp_conn *Conn, char *UserName) {
    char Command[MaxSentLine];
    u32 CommandLength = snprintf(Command, arrayCount(Command), "USER %s\r\n", UserName);
    Conn->LastCommand = FtpCmd_User;

    send(Conn->CtrlSocket, Command, CommandLength, 0);
}

static inline void sendPassword(ftp_conn *Conn, char *Password) {
    char Command[MaxSentLine];
    u32 CommandLength = snprintf(Command, arrayCount(Command), "PASS %s\r\n", Password);
    Conn->LastCommand = FtpCmd_Password;

    send(Conn->CtrlSocket, Command, CommandLength, 0);
}

static inline void sendType(ftp_conn *Conn) {
    char Command[] = "TYPE I\r\n";
    Conn->LastCommand = FtpCmd_Type;

    send(Conn->CtrlSocket, Command, arrayCount(Command)-1, 0);
}

static inline void sendPassive(ftp_conn *Conn) {
    char Command[] = "PASV\r\n";
    Conn->LastCommand = FtpCmd_Passive;

    send(Conn->CtrlSocket, Command, arrayCount(Command)-1, 0);
}

static inline void sendDirListAlt(ftp_conn *Conn) {
    if(Conn->DataSocket == -1) {
        Conn->CommandWaitingForData = FtpCmd_DirListAlt;
        sendPassive(Conn);
    }
    else {
        char Command[] = "LIST\r\n";
        Conn->LastCommand = FtpCmd_DirListAlt;

        send(Conn->CtrlSocket, Command, arrayCount(Command)-1, 0);
    }
}

static inline void sendDirList(ftp_conn *Conn) {
    if(Conn->UseAltDirListing) {
        sendDirListAlt(Conn);
        return;
    }

    if(Conn->DataSocket == -1) {
        Conn->CommandWaitingForData = FtpCmd_DirList;
        sendPassive(Conn);
    }
    else {
        char Command[] = "MLSD\r\n";
        Conn->LastCommand = FtpCmd_DirList;

        send(Conn->CtrlSocket, Command, arrayCount(Command)-1, 0);
    }
}

typedef struct  {
    u32 StartOffset;
    u32 Length;
} link_file_name_result;
static inline link_file_name_result getLinkFileName(ftp_conn *Conn, char *Name, u32 Length) {
    link_file_name_result Result = { .Length = Length };

    bool HasQuotes = false;
    for(u32 Idx = 0; Idx < Length; ++Idx) {
        if(Idx == 0) {
            if(Name[Idx] == '\'') {
                HasQuotes = true;
                Result.StartOffset = 1;
            }
        }
        else {
            if(HasQuotes) {
                if(Name[Idx] == '\'') {
                    Result.Length = Idx - Result.StartOffset;
                    break;
                }
            }
            else if(Conn->UseAltDirListing) {
                if(Name[Idx] == ' ') {
                    Result.Length = Idx - Result.StartOffset;
                    break;
                }
            }
        }
    }

    return Result;
}

static inline void sendChangeDir(ftp_conn *Conn, char *NewDirectory) {
    link_file_name_result LinkName = getLinkFileName(Conn, NewDirectory, strlen(NewDirectory));

    char Command[MaxSentLine];
    u32 CommandLength = snprintf(Command, arrayCount(Command), "CWD %.*s\r\n",
                                 LinkName.Length, NewDirectory + LinkName.StartOffset);
    Conn->LastCommand = FtpCmd_ChangeDir;

    send(Conn->CtrlSocket, Command, CommandLength, 0);
}

static inline void sendParentDirectory(ftp_conn *Conn) {
    sendChangeDir(Conn, "..");
}

static inline void sendRetrieve(ftp_conn *Conn) {
    if(Conn->DataSocket == -1) {
        Conn->CommandWaitingForData = FtpCmd_Retrieve;
        sendPassive(Conn);
    }
    else {
        char Command[MaxSentLine];
        u32 CommandLength = snprintf(Command, arrayCount(Command), "RETR %s\r\n", Conn->FileToRetrieve);
        Conn->LastCommand = FtpCmd_Retrieve;

        send(Conn->CtrlSocket, Command, CommandLength, 0);
    }
}

static inline void sendQuit(ftp_conn *Conn) {
    char Command[] = "QUIT\r\n";
    Conn->LastCommand = FtpCmd_Quit;

    send(Conn->CtrlSocket, Command, arrayCount(Command)-1, 0);
}

static bool processResponse(ftp_conn *Conn, ftp_code Code, char *Args, u32 ArgsLength) {
    bool Result = true;
    if(Code == FtpCode_DataClosed) {
        return Result;
    }

    ftp_command LastCommand = Conn->LastCommand;
    Conn->LastCommand = FtpCmd_Noop;
    *Conn->ErrorMsg = 0;

    switch(LastCommand) {
        case FtpCmd_Noop: {
            switch(Code) {
                case FtpCode_ServiceAlmostRdy: {} break;
                case FtpCode_ReadyForUser: {
                    if(*Conn->User) {
                        sendUser(Conn, Conn->User);
                    }
                    else {
                        sendType(Conn);
                    }
                } break;

                case FtpCode_DataClosed: {} break;

                case FtpCode_ServiceNotAvailable: {
                    writeErrorMsg(Conn, "Session terminated by the server.");
                    Result = false;
                } break;

                default: {
                    writeErrorMsg(Conn, "Unexpected response code received.");
                    Result = false;
                }
            }
        } break;

        case FtpCmd_User: {
            switch(Code) {
                case FtpCode_LoggedIn: {
                    sendType(Conn);
                } break;

                case FtpCode_NeedPassword: {
                    if(*Conn->Password) {
                        sendPassword(Conn, Conn->Password);
                    }
                    else {
                        writeErrorMsg(Conn, "Password is needed for this user!");
                        Result = false;
                    }
                } break;

                default: {
                    writeErrorMsg(Conn, "Error occurred while providing user name.");
                    Result = false;
                } break;
            }
        } break;

        case FtpCmd_Password: {
            switch(Code) {
                case FtpCode_LoggedIn: {
                    sendType(Conn);
                } break;

                default: {
                    writeErrorMsg(Conn, "Invalid user/password.");
                    Result = false;
                } break;
            }
        } break;

        case FtpCmd_Type: {
            switch(Code) {
                case FtpCode_Okay: {
                    Conn->State = Ftp_Connected;
                } break;

                case FtpCode_NotLoggedIn: {
                    writeErrorMsg(Conn, "Authentication required.");
                    Result = false;
                } break;

                default: {
                    writeErrorMsg(Conn, "Could not set type.");
                    Result = false;
                } break;
            }
        } break;

        case FtpCmd_Passive: {
            switch(Code) {
                case FtpCode_PassiveMode: {
                    u32 Iter = 0;
                    for(; Iter < ArgsLength; ++Iter) {
                        if(Args[Iter] == '(') {
                            ++Iter;
                            break;
                        }
                    }

                    u8 Parts[6] = {};
                    for(u32 PartIdx = 0; PartIdx < arrayCount(Parts); ++PartIdx) {
                        for(;; ++Iter) {
                            char Char = Args[Iter];

                            if(Char == ',') {
                                ++Iter;
                                break;
                            }
                            else if(Char == ')' || Iter == ArgsLength) {
                                if(PartIdx == arrayCount(Parts)-1) {
                                    break;
                                }
                                else {
                                    goto PassiveSyntaxErr;
                                }
                            }
                            else if(Char >= '0' && Char <= '9') {
                                Parts[PartIdx] = Parts[PartIdx]*10 + (Char-'0');
                            }
                            else {
                                goto PassiveSyntaxErr;
                            }
                        }
                    }

                    {
                        struct sockaddr_in Address = {
                            .sin_family = AF_INET,
                            .sin_port   =  htons(Parts[4] << 8  | Parts[5] << 0),
                            .sin_addr   = {htonl(Parts[0] << 24 | Parts[1] << 16 | Parts[2] << 8 | Parts[3] << 0)},
                        };

                        Conn->DataSocket = socket(Address.sin_family, SOCK_STREAM, 0);
                        if(Conn->DataSocket >= 0) {
                            int Err = connect(Conn->DataSocket, (sockaddr *)&Address, sizeof(Address));
                            if(Err == 0) {
                                fcntl(Conn->DataSocket, F_SETFL, O_NONBLOCK);
                            }
                            else {
                                writeErrorMsg(Conn, "Could not establish data connection (%d).", errno);
                                Result = false;
                            }
                        }
                        else {
                            writeErrorMsg(Conn, "Error creating data socket.");
                            Result = false;
                        }

                        if(Result) {
                            switch(Conn->CommandWaitingForData) {
                                case FtpCmd_DirList: {
                                    sendDirList(Conn);
                                } break;

                                case FtpCmd_DirListAlt: {
                                    sendDirListAlt(Conn);
                                } break;

                                case FtpCmd_Retrieve: {
                                    sendRetrieve(Conn);
                                } break;

                                default: {} break;
                            }

                            Conn->CommandWaitingForData = FtpCmd_Noop;
                        }
                    }
                    break;

                  PassiveSyntaxErr:
                    writeErrorMsg(Conn, "Invalid passive response format.");
                } break;

                case FtpCode_NotLoggedIn: {
                    writeErrorMsg(Conn, "Authentication required.");
                    Result = false;
                } break;

                default: {
                    writeErrorMsg(Conn, "Error establishing passive connection.");
                    Result = false;
                } break;
            }
        } break;

        case FtpCmd_DirList: {
            if(Code == FtpCode_OpeningData || Code == FtpCode_TransferStarting || Code == FtpCode_DataClosed)
            {
                Conn->DataCommand = FtpCmd_DirList;
            }
            else if(Code == FtpCode_SyntaxError) {
                Conn->UseAltDirListing = true;
                sendDirListAlt(Conn);
            }
            else {
                writeErrorMsg(Conn, "Error listing directory.");
                Result = false;
            }
        } break;

        case FtpCmd_DirListAlt: {
            if(Code == FtpCode_OpeningData || Code == FtpCode_TransferStarting || Code == FtpCode_DataClosed)
            {
                Conn->DataCommand = FtpCmd_DirListAlt;
            }
            else {
                writeErrorMsg(Conn, "Error listing directory.");
                Result = false;
            }
        } break;

        case FtpCmd_Retrieve: {
            if(Code == FtpCode_OpeningData || Code == FtpCode_TransferStarting || Code == FtpCode_DataClosed)
            {
                Conn->DataCommand = FtpCmd_Retrieve;
            }
            else {
                writeErrorMsg(Conn, "Error retrieving file.");
                close(Conn->DestFile);
                Conn->DestFile = -1;
            }
        } break;

        case FtpCmd_ChangeDir: {
            switch(Code) {
                case FtpCode_FileActionCompleted: {
                    Conn->HasDir = false;
                } break;

                case FtpCode_FileUnavailable: {
                    writeErrorMsg(Conn, "Could not open directory.");
                } break;

                default: {
                    writeErrorMsg(Conn, "Error while opening directory.");
                    Result = false;
                } break;
            }
        } break;

        case FtpCmd_Quit: {
            Result = false;
        } break;

        invalidDefaultCase();
    }

    if(!Result) {
        resetFtpConn(Conn);
    }

    return Result;
}

static void setLoading(ftp_conn *Conn) {
    Conn->State = Ftp_Loading;
    Conn->FramesWaiting = 0;
}

void handleRxData(ftp_conn *Conn) {
    ftp_command Cmd = Conn->DataCommand;
    Conn->DataCommand = FtpCmd_Noop;

    switch(Cmd) {
        case FtpCmd_DirList: {
            for(int Idx = 0; Idx < bufLength(Conn->DirList); ++Idx) {
                free(Conn->DirList[Idx].Name);
            }
            bufClear(Conn->DirList);

            bool Dir = false;

            u8 ParamType = 0; // 0 - parsing, 1 - type, 2 - name, 3 - ignored
            u32 Start = 0;

            for(u32 Idx = 0; Idx < Conn->DataLength; ++Idx) {
                if(ParamType == 0) {
                    if(Conn->DataRx[Idx] == '=') {
                        if(strncmp((char *)(Conn->DataRx + Start), "type", 4) == 0) {
                            ParamType = 1;
                        }
                        else {
                            ParamType = 3;
                        }
                    }
                    else if(Conn->DataRx[Idx] == ' ') {
                        ParamType = 2;
                        Start = Idx + 1;
                    }
                }
                else if(ParamType == 1) {
                    if(Conn->DataRx[Idx] == 'd') {
                        Dir = true;
                    }
                    else {
                        Dir = false;
                    }

                    ParamType = 3;
                }
                else if(ParamType == 2) {
                    if(Conn->DataRx[Idx] == '\r') {
                        u32 Length = Idx - Start;
                        char *Name = (char *)malloc((Length + 1)*sizeof(*Name));
                        strncpy(Name, (char *)(Conn->DataRx + Start), Length);
                        Name[Length] = 0;
                        bufPush(Conn->DirList, (dir_item){Name, Dir});

                        ParamType = 0;
                        Start = Idx + 2;
                        Dir = false;
                    }
                }
                else {
                    if(Conn->DataRx[Idx] == ';' || Conn->DataRx[Idx] == '\n') {
                        ParamType = 0;
                        Start = Idx+1;
                    }
                }
            }

            Conn->HasDir = true;
        } break;

        case FtpCmd_DirListAlt: {
            for(int Idx = 0; Idx < bufLength(Conn->DirList); ++Idx) {
                free(Conn->DirList[Idx].Name);
            }
            bufClear(Conn->DirList);

            u8 Dir = 0; // 0 - file, 1 - dir, 2 - check if dir

            u8 Type = 0; // 0 - first letter, i - column i
            u32 Start = 0;
            bool SeenSpace = false;

            for(u32 Idx = 0; Idx < Conn->DataLength; ++Idx) {
                if(Type == 0) {
                    if(Conn->DataRx[Idx] == '-') {
                        Dir = 0;
                    }
                    else if(Conn->DataRx[Idx] == 'd') {
                        Dir = 1;
                    }
                    else {
                        Dir = 2;
                    }

                    Type = 1;
                }
                else if(Type < 9) {
                    if(!SeenSpace) {
                        if(Conn->DataRx[Idx] == ' ') {
                            SeenSpace = true;
                        }
                    }
                    else {
                        if(Conn->DataRx[Idx] != ' ') {
                            ++Type;
                            SeenSpace = false;
                            if(Type == 9) {
                                Start = Idx;
                            }
                        }
                    }
                }
                else {
                    if(Conn->DataRx[Idx] == '\r')  {
                        if(Dir == 2) {
                            if(Conn->DataRx[Idx-1] == '/') {
                                Dir = 1;
                            }
                            else {
                                Dir = 0;
                            }
                        }

                        u32 Length = Idx - Start;
                        char *Name = (char *)malloc((Length + 1)*sizeof(*Name));
                        strncpy(Name, (char *)(Conn->DataRx + Start), Length);
                        Name[Length] = 0;
                        bufPush(Conn->DirList, (dir_item){Name, (bool)Dir});
                    }
                    else if(Conn->DataRx[Idx] == '\n') {
                        Type = 0;
                        Start = Idx + 1;
                    }
                }

                Conn->HasDir = true;
            }
        } break;

        case FtpCmd_Retrieve: {
            if(write(Conn->DestFile, Conn->DataRx, Conn->DataLength) < 0) {
                writeErrorMsg(Conn, "Could not write to file. %s", strerror(errno));
            }
            close(Conn->DestFile);
            Conn->DestFile = -1;
        } break;

        default: {} break;
    }
}

int main() {
    glfwSetErrorCallback(glfwErrorCallback);
    if(!glfwInit()) {
        return 1;
    }

    GLFWwindow* Window = glfwCreateWindow(1280, 720, "FTP Client", NULL, NULL);
    if(Window == NULL) {
        return 1;
    }
    glfwMakeContextCurrent(Window);
    glfwSwapInterval(1);

    if(glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize OpenGL loader!\n");
        return 1;
    }

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& Io = ImGui::GetIO();
    Io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;

    ImGui::StyleColorsDark();

    ImGui_ImplGlfw_InitForOpenGL(Window, true);
    ImGui_ImplOpenGL2_Init();

    ftp_conn Conn = { .CtrlSocket = -1, .DataSocket = -1 };

    char ServerInput[128] = {};
    char PortInput[10] = {};

    s32  ProgressRemainingFrames = ProgressFrames;
    char Progress[4] = {};

    while(!glfwWindowShouldClose(Window)) {
        glfwPollEvents();

        int DisplayWidth, DisplayHeight;
        glfwGetFramebufferSize(Window, &DisplayWidth, &DisplayHeight);

        ImGui_ImplOpenGL2_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        struct pollfd PollFds[2] = {
            {Conn.CtrlSocket, POLLIN},
            {Conn.DataSocket, POLLIN},
        };

        if(poll(PollFds, arrayCount(PollFds), 0) < 0) {
            writeErrorMsg(&Conn, "Could not run poll.");
        }

        if(PollFds[0].revents & POLLIN) {
            while(Conn.CtrlSocket >= 0) {
                s32 ReceivedBytes = recv(Conn.CtrlSocket, Conn.CtrlRx + Conn.CtrlRxLength,
                                         arrayCount(Conn.CtrlRx) - Conn.CtrlRxLength, 0);

                if(ReceivedBytes <= 0) {
                    if(ReceivedBytes == 0) {
                        resetFtpConn(&Conn);
                    }
                    else if(errno != EAGAIN && errno != EWOULDBLOCK) {
                        writeErrorMsg(&Conn, "Error receiving data from socket.");
                        resetFtpConn(&Conn);
                    }

                    break;
                }

                u32 StartingIdx = Conn.CtrlRxLength ? Conn.CtrlRxLength : 1;
                Conn.CtrlRxLength += ReceivedBytes;

                for(u32 Idx = StartingIdx;
                    Idx < Conn.CtrlRxLength;
                    ++Idx)
                {
                    if(Conn.CtrlRx[Idx-1] == '\r' && Conn.CtrlRx[Idx] == '\n') {
                        if(Conn.CtrlRx[0] >= '0' && Conn.CtrlRx[0] <= '9' &&
                           Conn.CtrlRx[1] >= '0' && Conn.CtrlRx[1] <= '9' &&
                           Conn.CtrlRx[2] >= '0' && Conn.CtrlRx[2] <= '9' && Conn.CtrlRx[3] == ' ')
                        {
                            ftp_code Code = (ftp_code)((Conn.CtrlRx[0]-'0')*100 +
                                                       (Conn.CtrlRx[1]-'0')*10  +
                                                       (Conn.CtrlRx[2]-'0'));

                            u32 MessageLength  = Idx-5;
                            char *MessageStart = (char *)(Conn.CtrlRx+4);

                            if(!processResponse(&Conn, Code, MessageStart, MessageLength)) {
                                goto CancelCtrl;
                            }
                        }
                        else {
                            writeErrorMsg(&Conn, "Server response doesn't start with the response code.");
                        }

                        u32 BytesToRemove = Idx+1;
                        Idx = 0;

                        Conn.CtrlRxLength -= BytesToRemove;
                        if(Conn.CtrlRxLength) {
                            memmove(Conn.CtrlRx, Conn.CtrlRx + BytesToRemove, Conn.CtrlRxLength);
                        }
                    }
                }
            }

          CancelCtrl:;
        }

        if(PollFds[1].revents & POLLIN) {
            while(Conn.DataSocket >= 0) {
                if(Conn.DataLength + 1 >= Conn.DataCap) {
                    Conn.DataCap = Conn.DataCap ? 2*Conn.DataCap : 1024;
                    Conn.DataRx = (u8 *)realloc(Conn.DataRx, Conn.DataCap);
                    if(!Conn.DataRx) {
                        writeErrorMsg(&Conn, "Could not allocate data receive buffer.");
                        resetFtpConn(&Conn);

                        break;
                    }
                }

                s32 ReceivedBytes = recv(Conn.DataSocket, Conn.DataRx + Conn.DataLength,
                                         Conn.DataCap - Conn.DataLength - 1, 0);

                if(ReceivedBytes <= 0) {
                    if(ReceivedBytes == 0) {
                        handleRxData(&Conn);
                        close(Conn.DataSocket);
                        Conn.DataSocket = -1;
                        Conn.DataLength = 0;
                    }
                    else if(errno != EAGAIN && errno != EWOULDBLOCK) {
                        writeErrorMsg(&Conn, "Error receiving data from socket.");
                        resetFtpConn(&Conn);
                    }

                    break;
                }
                else {
                    Conn.DataLength += ReceivedBytes;

                    if(Conn.DataCommand == FtpCmd_Retrieve && Conn.DataLength >= FileChunkSizes) {
                        if(write(Conn.DestFile, Conn.DataRx, Conn.DataLength) < 0) {
                            writeErrorMsg(&Conn, "Could not write to file. %s", strerror(errno));
                        }

                        Conn.DataLength = 0;
                    }
                }
            }
        }

        if(Conn.State == Ftp_Disconnected) {
            ImGui::SetNextWindowPos({(float)DisplayWidth/2, (float)DisplayHeight/2}, 0, {0.5f, 0.5f});
            ImGui::Begin("FTP login", 0,
                         ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse);

            ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.345f);
            ImGui::InputText("Server", ServerInput, IM_ARRAYSIZE(ServerInput)); ImGui::PopItemWidth();
            ImGui::SameLine(); ImGui::PushItemWidth(ImGui::GetWindowWidth() * 0.2f);
            ImGui::InputText("Port", PortInput, IM_ARRAYSIZE(PortInput)); ImGui::PopItemWidth();

            ImGui::InputText("User", Conn.User, IM_ARRAYSIZE(Conn.User));
            ImGui::InputText("Password", Conn.Password, IM_ARRAYSIZE(Conn.Password), ImGuiInputTextFlags_Password);

            if(*Conn.ErrorMsg) {
                ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.8f, 0.0f, 0.0f, 1.0f));
                ImGui::TextUnformatted(Conn.ErrorMsg);
                ImGui::PopStyleColor();
            }

            if(ImGui::Button("Login")) {
                char *Port = *PortInput ? PortInput : (char *)"21";

                struct addrinfo Hints = {
                    .ai_family   = AF_INET, // NOTE(nox): Only IPv4
                    .ai_socktype = SOCK_STREAM,
                };

                int ErrorValue;
                struct addrinfo *Result = 0;
                if((ErrorValue = getaddrinfo(ServerInput, Port, &Hints, &Result)) == 0 && Result) {
                    Conn.CtrlSocket = socket(Result->ai_family, Result->ai_socktype, Result->ai_protocol);

                    if(Conn.CtrlSocket >= 0) {
                        ErrorValue = connect(Conn.CtrlSocket, Result->ai_addr, Result->ai_addrlen);
                        if(ErrorValue == 0) {
                            setLoading(&Conn);
                            fcntl(Conn.CtrlSocket, F_SETFL, O_NONBLOCK);
                        }
                        else {
                            resetFtpConn(&Conn);
                            writeErrorMsg(&Conn, "Could not connect (%d).", errno);
                        }
                    }
                    else {
                        writeErrorMsg(&Conn, "Could not create socket.");
                    }
                }
                else {
                    writeErrorMsg(&Conn, "Invalid address: %s", gai_strerror(ErrorValue));
                }

                freeaddrinfo(Result);
            }

            ImGui::End();
        }
        else if(Conn.State == Ftp_Loading) {
            if(Conn.FramesWaiting >= TimeoutFrames) {
                resetFtpConn(&Conn);
                writeErrorMsg(&Conn, "Timed out waiting for response.");
            }
            else {

                if(ProgressRemainingFrames-- <= 0) {
                    ProgressRemainingFrames = ProgressFrames;

                    if(!Progress[0]) {
                        Progress[0] = '.';
                        Progress[1] = 0;
                    }
                    else if(!Progress[1]) {
                        Progress[1] = '.';
                        Progress[2] = 0;
                    }
                    else if(!Progress[2]) {
                        Progress[2] = '.';
                        Progress[3] = 0;
                    }
                    else {
                        Progress[0] = 0;
                    }
                }

                ImGui::SetNextWindowPos({(float)DisplayWidth/2, (float)DisplayHeight/2}, 0, {0.5f, 0.5f});
                ImGui::Begin("Connecting...", 0,
                             ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse);
                ImGui::TextUnformatted("Connecting to FTP server"); ImGui::SameLine();
                ImGui::TextUnformatted(Progress);
                ImGui::End();

                ++Conn.FramesWaiting;

            }
        }
        else if(Conn.State == Ftp_Connected) {
            ImGui::Begin("FTP session");

            if(ImGui::Button("Logout") && !ftpIsWaiting(&Conn)) {
                sendQuit(&Conn);
            }

            ImGui::SameLine();
            if(ImGui::Button("Parent directory") && !ftpIsWaiting(&Conn)) {
                sendParentDirectory(&Conn);
            }

            if(Conn.ErrorMsg) {
                ImGui::SameLine();
                ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.8f, 0.0f, 0.0f, 1.0f));
                ImGui::TextUnformatted(Conn.ErrorMsg);
                ImGui::PopStyleColor();
            }

            if(ftpIsWaiting(&Conn)) {
                ImGui::SameLine();
                ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(0.870f, 0.588f, 0.192f, 1.0f));
                ImGui::TextUnformatted("Waiting for response...");
                ImGui::PopStyleColor();
            }

            if(Conn.HasDir) {
                for(int Idx = 0; Idx < bufLength(Conn.DirList); ++Idx) {
                    dir_item *Item = Conn.DirList + Idx;

                    if(Item->Dir) {
                        char OpenText[30];
                        snprintf(OpenText, arrayCount(OpenText), "Open directory##%d", Idx);
                        if(ImGui::Button(OpenText) && !ftpIsWaiting(&Conn)) {
                            sendChangeDir(&Conn, Item->Name);
                        }
                    }
                    else {
                        char DownloadText[30];
                        snprintf(DownloadText, arrayCount(DownloadText), "Download##%d", Idx);
                        if(ImGui::Button(DownloadText) && !ftpIsWaiting(&Conn)) {
                            link_file_name_result LinkName = getLinkFileName(&Conn, Item->Name, strlen(Item->Name));
                            char Name[1024];
                            snprintf(Name, arrayCount(Name), "%.*s",
                                     LinkName.Length, Item->Name + LinkName.StartOffset);

                            Conn.DestFile = open(Name, O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
                            if(Conn.DestFile < 0) {
                                writeErrorMsg(&Conn, "Could not open file to write. File: %s", Name);
                            }
                            else {
                                Conn.FileToRetrieve = Item->Name;
                                sendRetrieve(&Conn);
                            }
                        }
                    }

                    ImGui::SameLine();
                    ImGui::TextUnformatted(Item->Name);
                }
            }
            else if(!ftpIsWaiting(&Conn)) {
                sendDirList(&Conn);
            }

            ImGui::End();
        }
        else {
            invalidCodePath();
        }

        ImGui::Render();
        glViewport(0, 0, DisplayWidth, DisplayHeight);
        glClearColor(0.45f, 0.55f, 0.60f, 1.00f);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL2_RenderDrawData(ImGui::GetDrawData());

        glfwSwapBuffers(Window);
    }

    ImGui_ImplOpenGL2_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    glfwDestroyWindow(Window);
    glfwTerminate();

    return 0;
}
