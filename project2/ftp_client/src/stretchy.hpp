typedef struct {
    int Capacity;
    int Length;
    u8 Data[];
} buf_header;

#define bufHeader_(B) (((buf_header *)(B)) - 1)
#define bufMaybeGrow_(B) (!(B) || bufLength(B) >= bufCapacity(B)) ? (*(void **)&(B)) = bufGrow_(B, sizeof(*(B))) : 0

#define bufPush(B, ...) (bufMaybeGrow_(B), (B)[bufHeader_(B)->Length++] = __VA_ARGS__)
#define bufCapacity(B) ((B) ? bufHeader_(B)->Capacity : 0)
#define bufLength(B) ((B) ? bufHeader_(B)->Length : 0)

#define bufClear(B) ((B) ? bufHeader_(B)->Length = 0 : 0)
#define bufFree(B) ((B) ? free(bufHeader_(B)), (B) = 0 : 0)

void *bufGrow_(void *B, size_t ItemSize) {
    buf_header *Header = bufHeader_(B);
    int DoubleCap = B ? 2*Header->Capacity : 20;

    buf_header *NewHeader = (buf_header *)realloc(B ? Header : 0, DoubleCap * sizeof(*Header) * ItemSize);

    if(NewHeader) {
        if(!B) {
            NewHeader->Length = 0;
        }
        NewHeader->Capacity = DoubleCap;

        return NewHeader->Data;
    }
    else {
        abort();
    }
}
