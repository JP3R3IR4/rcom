#!/usr/bin/env bash
cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1
python -m pyftpdlib -V -D --port 2121 --directory=$(pwd) --username=user --password=pass
