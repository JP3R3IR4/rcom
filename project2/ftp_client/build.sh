#!/usr/bin/env bash
cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1
. setup.sh

g++ $Flags "$SrcDir/main.cpp" -o "$BuildDir/ftp_client"
