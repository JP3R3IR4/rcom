#!/usr/bin/env bash
cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1
. setup.sh

gcc $DepsFlags "$DepsDir/glew/src/glew.c" -c -o "$DepsBuildDir/glew.o"

gcc $DepsFlags "$DepsDir/glfw.c" -c -o "$DepsBuildDir/glfw.o"

g++ $DepsFlags "$DepsDir/imgui/imgui.cpp" -c -o "$DepsBuildDir/imgui.o"
g++ $DepsFlags "$DepsDir/imgui/imgui_impl_glfw.cpp" -c -o "$DepsBuildDir/imgui_impl_glfw.o"
g++ $DepsFlags "$DepsDir/imgui/imgui_impl_opengl2.cpp" -c -o "$DepsBuildDir/imgui_impl_opengl2.o"
g++ $DepsFlags "$DepsDir/imgui/imgui_demo.cpp" -c -o "$DepsBuildDir/imgui_demo.o"
g++ $DepsFlags "$DepsDir/imgui/imgui_draw.cpp" -c -o "$DepsBuildDir/imgui_draw.o"
g++ $DepsFlags "$DepsDir/imgui/imgui_widgets.cpp" -c -o "$DepsBuildDir/imgui_widgets.o"
