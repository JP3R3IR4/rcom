#if !defined(CONFIG_H)
#define CONFIG_H

#define DebugMsgs 0
#define ErrorTest 0
#define PropagationTest 0

#if ErrorTest
double  HeaderErrorProbability = 1e-3;
double PayloadErrorProbability = 1e-2;
#endif

#if PropagationTest
#include <time.h>
const struct timespec PropagationTime = {
    .tv_nsec = 50000,
};
#endif

#endif // CONFIG_H
