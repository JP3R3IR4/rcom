#if !defined(PROTOCOL_INTERNAL_H)
#define PROTOCOL_INTERNAL_H

#include "util.h"
#include "config.h"

#include <assert.h>
#include <termio.h>

enum {
    DefaultTimeout = 3000,
    RetryTimes     = 3,
};

typedef enum {
    Rx_Command,
    Rx_Response,
    Rx_Any,
} rx_type;

enum {
    Info_HeaderSize                = 4,
    Info_MaximumStuffedPayloadSize = 2*llMaximumPayloadSize,
    Info_MaximumStuffedTrailerSize = 2*1 + 1,
    Info_MaximumFrameSize          = (Info_HeaderSize + Info_MaximumStuffedPayloadSize + Info_MaximumStuffedTrailerSize),

    Other_FrameSize = 5,

    MaximumFrameSize = Info_MaximumFrameSize,
};

enum { DelimiterFlag = 0x7E };

typedef enum {
    Addr_Tx = 0x03,
    Addr_Rx = 0x01,
} address;

enum{
    Control_TypeMask = 0x0F,

    Control_SeqSShift = 6,
    Control_SeqSMask  = 1 << Control_SeqSShift,

    Control_SeqRShift = 7,
    Control_SeqRMask  = 1 << Control_SeqRShift,
};

#define FrameTypes(W) W(Info, 0x0),             \
        W(Set,   0x3),                          \
        W(Disc,  0xB),                          \
        W(UAck,  0x7),                          \
        W(RxRdy, 0x5),                          \
        W(Rej,   0x1)

typedef enum {
#define writer(Name, Code) FrameType_##Name = Code
    FrameTypes(writer)
#undef writer
} frame_type;

enum {
    EscapeByte = 0x7D,
    EscapeXor  = 0x20,
};

typedef s64 milli;

typedef struct {
    union {
        u8 Info[Info_MaximumFrameSize];
        u8 Other[Other_FrameSize];

        u8 Data[MaximumFrameSize];
    };
    int Size;
} frame;

typedef struct {
    bool Valid;
    address Addr;
    frame_type Type;
    u8 SequenceBit;
    u32 PayloadSize;
    u8 Payload[llMaximumPayloadSize+1];
} parsed_frame;

typedef enum {
    PState_Start,

    PState_Flag,
    PState_Addr,
    PState_Control,

    PState_OtherBcc,

    PState_ReceivePayload,
    PState_ReceivePayloadEscape,
} parse_state;

typedef struct ll_conn {
    struct termios SavedSerialConfig;
    struct {
        u32 RxHead;
        u32 RxTail;
        u8 RxBuf[MaximumFrameSize*2];

        parsed_frame RxFrame;
        u32 RxIdx;
        parse_state RxState;
        u8 HeaderBcc;
        u8 PayloadBcc;
    };
    ll_endpoint_type EndpointType;
    int SerialFileDesc;
    u8 SequenceBit;
    bool Established;

#if ErrorTest
    u64 TotalCount;
    u64 SuccessfulCount;
    u64 FailedHeaderCount;
    u64 FailedPayloadCount;
#endif
} ll_conn;

static inline u32 connRxMod(ll_conn *C, u32 Idx) {
    return Idx % arrayCount(C->RxBuf);
}

static inline u32 connRxInc(ll_conn *C, u32 Num) {
    return connRxMod(C, Num+1);
}

static inline u32 connRxFilled(ll_conn *C) {
    return connRxMod(C, (C->RxHead + arrayCount(C->RxBuf) - C->RxTail));
}

static inline s32 connRxUnfilled(ll_conn *C) {
    // NOTE(nox): Has to be implemented this way because of the edge case Head ≡ Tail.
    return (arrayCount(C->RxBuf)-1) - connRxFilled(C);
}

static inline void connRxInsert(ll_conn *C, u8 Byte) {
    assert(connRxUnfilled(C));
    C->RxBuf[C->RxHead] = Byte;
    C->RxHead = connRxMod(C, C->RxHead+1);
}

static inline u8 connRxGet(ll_conn *C, u32 Idx) {
    assert(Idx < arrayCount(C->RxBuf));
    return C->RxBuf[Idx];
}

#endif // PROTOCOL_INTERNAL_H
