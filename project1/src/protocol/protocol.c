#include "protocol.h"
#include "protocol_internal.h"

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

#if DebugMsgs
#define debugPrint(Fmt, ...) fprintf(stderr, "LL: " Fmt, ##__VA_ARGS__)

struct { u8 Code; char *Name; } FrameTypeLookup[] = {
#define writer(Name, Code) {Code, #Name}
    FrameTypes(writer)
#undef writer
};

static char *lookupFrameTypeName(u8 Code) {
    char *Result = 0;

    for(u32 Idx = 0; Idx < arrayCount(FrameTypeLookup); ++Idx) {
        if(FrameTypeLookup[Idx].Code == Code) {
            Result = FrameTypeLookup[Idx].Name;
            break;
        }
    }

    return Result;
}
#else
#define debugPrint(...)
#endif

static int setupSerialPort(ll_conn *Conn) {
    if(tcgetattr(Conn->SerialFileDesc, &Conn->SavedSerialConfig) == -1) {
        return -1;
    }

    struct termios TerminalConfig = {};
    TerminalConfig.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
    TerminalConfig.c_iflag = IGNPAR;
    TerminalConfig.c_oflag = 0;
    TerminalConfig.c_lflag = 0;

    TerminalConfig.c_cc[VTIME] = 1;
    TerminalConfig.c_cc[VMIN]  = 0;

    tcflush(Conn->SerialFileDesc, TCIOFLUSH);
    if(tcsetattr(Conn->SerialFileDesc, TCSANOW, &TerminalConfig) == -1) {
        return -1;
    }

    return 0;
}

static int restoreSerialPort(ll_conn *Conn) {
    if(tcsetattr(Conn->SerialFileDesc, TCSAFLUSH, &Conn->SavedSerialConfig) == -1) {
        return -1;
    }

    return 0;
}

static void fillInfoFrame(ll_conn *Conn, frame *Frame, u8 *Buf, u32 Size) {
    assert(Conn->EndpointType == llEndpoint_Tx);
    assert(Size <= llMaximumPayloadSize);

    u8 *Header = Frame->Info;
    Header[0] = DelimiterFlag;
    Header[1] = Addr_Tx;
    Header[2] = FrameType_Info | Conn->SequenceBit << Control_SeqSShift;
    Header[3] = Frame->Info[1] ^ Frame->Info[2];

    u8  PayloadBcc = 0;
    u32 UsedBytes  = 0;

    u8 *Payload = Header + 4;
    for(u32 Idx = 0; Idx < Size; ++Idx) {
        u8 Byte = Buf[Idx];

        PayloadBcc ^= Byte;

        if(Byte == DelimiterFlag || Byte == EscapeByte) {
            Payload[UsedBytes++] = EscapeByte;
            Byte ^= EscapeXor;
        }

        Payload[UsedBytes++] = Byte;
    }

    assert(UsedBytes <= Info_MaximumStuffedPayloadSize);

    u8 *Trailer = Payload + UsedBytes;
    u8 TrailerSize = 0;

    if(PayloadBcc == DelimiterFlag || PayloadBcc == EscapeByte) {
        Trailer[TrailerSize++] = EscapeByte;
        PayloadBcc ^= EscapeXor;
    }

    Trailer[TrailerSize++] = PayloadBcc;
    Trailer[TrailerSize++] = DelimiterFlag;

    Frame->Size = Info_HeaderSize + UsedBytes + TrailerSize;
}

static void fillOtherFrame(ll_conn *Conn, frame *Frame, frame_type Type, bool IsCommand) {
    Frame->Other[0] = DelimiterFlag;

    if(IsCommand) {
        Frame->Other[1] = Conn->EndpointType == llEndpoint_Tx ? Addr_Tx : Addr_Rx;
    }
    else {
        Frame->Other[1] = Conn->EndpointType == llEndpoint_Tx ? Addr_Rx : Addr_Tx;
    }

    Frame->Other[2] = Type;
    if(Type == FrameType_RxRdy || Type == FrameType_Rej) {
        Frame->Other[2] |= Conn->SequenceBit << Control_SeqRShift;
    }

    Frame->Other[3] = Frame->Other[1] ^ Frame->Other[2];
    Frame->Other[4] = DelimiterFlag;

    Frame->Size = Other_FrameSize;
}

static int sendFrame(ll_conn *Conn, frame *Frame) {
    int Result = 0;

    u8 Type = Frame->Data[2] & Control_TypeMask;
    if(Type == FrameType_Info) {
        debugPrint("Sending frame of type %s, seq %d\n", lookupFrameTypeName(Type),
                   (Frame->Data[2] >> Control_SeqSShift) & 1);
    }
    else if(Type == FrameType_RxRdy || Type == FrameType_Rej) {
        debugPrint("Sending frame of type %s, seq %d\n", lookupFrameTypeName(Type),
                   (Frame->Data[2] >> Control_SeqRShift) & 1);
    }
    else {
        debugPrint("Sending frame of type %s\n", lookupFrameTypeName(Type));
    }

    u8 *ToWrite = Frame->Info;
    int Size = Frame->Size;

    for(int WrittenBytes = 0; WrittenBytes < Size;) {
        int NewWrittenBytes = write(Conn->SerialFileDesc, ToWrite+WrittenBytes, Size-WrittenBytes);

        if(NewWrittenBytes < 0) {
            Result = -1;
            break;
        }

        WrittenBytes += NewWrittenBytes;
    }

    return Result;
}

static int receiveBytes(ll_conn *Conn) {
    int Result = 0;

    u32 AvailableSpace = connRxUnfilled(Conn);

    if(AvailableSpace) {
        u8 TempBuf[arrayCount(Conn->RxBuf)];
        Result = read(Conn->SerialFileDesc, TempBuf, AvailableSpace);

        if(Result > 0) {
            for(int BytesReceived = 0; BytesReceived < Result; ++BytesReceived) {
                connRxInsert(Conn, TempBuf[BytesReceived]);
            }
        }
    }

    return Result;
}

static inline void dropFrame(ll_conn *Conn) {
    debugPrint("Dropping frame\n");

    if(Conn->RxBuf[Conn->RxTail] == DelimiterFlag) {
        Conn->RxTail = connRxInc(Conn, Conn->RxTail);
    }

    for(; connRxFilled(Conn); Conn->RxTail = connRxInc(Conn, Conn->RxTail)) {
        if(Conn->RxBuf[Conn->RxTail] == DelimiterFlag) {
            break;
        }
    }

    Conn->RxState = PState_Start;
    Conn->RxIdx = Conn->RxTail;
}

static inline void dropFrameWhileParsing(ll_conn *Conn, u8 Byte) {
    debugPrint("  -> Dropping current frame while parsing\n");
    if(Byte == DelimiterFlag) {
        Conn->RxState = PState_Flag;
        Conn->RxTail = Conn->RxIdx;
    }
    else {
        Conn->RxState = PState_Start;
        Conn->RxTail = connRxInc(Conn, Conn->RxIdx);
    }
}

static inline void rejectInfoFrame(ll_conn *Conn, bool Duplicate) {
    assert(Conn->RxFrame.Type == FrameType_Info);

    frame FrameToSend;

    if(Duplicate) {
        assert(Conn->SequenceBit != Conn->RxFrame.SequenceBit);
        debugPrint("  -> Duplicate Info frame, seq %d\n", Conn->RxFrame.SequenceBit);

        fillOtherFrame(Conn, &FrameToSend, FrameType_RxRdy, false);
    }
    else {
        assert(Conn->SequenceBit == Conn->RxFrame.SequenceBit);
        debugPrint("  -> Rejecting Info frame, seq %d\n", Conn->RxFrame.SequenceBit);

        fillOtherFrame(Conn, &FrameToSend, FrameType_Rej, false);
    }

#if PropagationTest
    nanosleep(&PropagationTime, 0);
#endif

    sendFrame(Conn, &FrameToSend);
    dropFrameWhileParsing(Conn, Conn->RxBuf[Conn->RxIdx]);
}

static inline void finalizeRxFrame(ll_conn *Conn) {
    assert(Conn->RxState == PState_OtherBcc || Conn->RxState == PState_ReceivePayload);
    assert(Conn->RxFrame.Type != FrameType_Info || Conn->SequenceBit == Conn->RxFrame.SequenceBit);

    if(Conn->RxFrame.Type == FrameType_Info  ||
       Conn->RxFrame.Type == FrameType_RxRdy || Conn->RxFrame.Type == FrameType_Rej)
    {
        debugPrint("  -> Parse done: %s, seq %d\n", lookupFrameTypeName(Conn->RxFrame.Type),
                   Conn->RxFrame.SequenceBit);
    }
    else {
        debugPrint("  -> Parse done: %s\n", lookupFrameTypeName(Conn->RxFrame.Type));
    }

    Conn->RxFrame.Valid = true;

    Conn->RxState = PState_Start;
    Conn->RxTail = connRxInc(Conn, Conn->RxIdx);

#if PropagationTest
    nanosleep(&PropagationTime, 0);
#endif

#if ErrorTest
    if(Conn->RxFrame.Type == FrameType_Info) {
        ++Conn->TotalCount;

        u32 HeaderErrorTest = lrand48();
        if(HeaderErrorTest < (u32)(1<<31) * HeaderErrorProbability) {
            debugPrint("Simulating \033[1;31mheader\033[0m error\n");
            Conn->RxFrame.Valid = false;
            ++Conn->FailedHeaderCount;
        }
        else {
            u32 PayloadErrorTest = lrand48();
            if(PayloadErrorTest < (u32)(1<<31) * PayloadErrorProbability) {
                debugPrint("Simulating \033[1;31mpayload\033[0m error\n");
                Conn->RxFrame.Valid = false;

                frame FrameToSend;
                fillOtherFrame(Conn, &FrameToSend, FrameType_Rej, false);
                sendFrame(Conn, &FrameToSend);

                ++Conn->FailedPayloadCount;
            }
            else {
                ++Conn->SuccessfulCount;
            }
        }
    }
#endif
}

static void tryParseFrame(ll_conn *Conn) {
    assert(!Conn->RxFrame.Valid);

    debugPrint("Parsing frame...\n");

    for(;
        !Conn->RxFrame.Valid && Conn->RxIdx != Conn->RxHead;
        Conn->RxIdx = connRxInc(Conn, Conn->RxIdx))
    {
        u8 Byte = connRxGet(Conn, Conn->RxIdx);
        switch(Conn->RxState) {
            case PState_Start: {
                Conn->RxTail = Conn->RxIdx;

                if(Byte == DelimiterFlag) {
                    Conn->RxState = PState_Flag;
                }
            } break;

            case PState_Flag: {
                Conn->HeaderBcc = 0;
                Conn->PayloadBcc = 0;
                Conn->RxFrame.SequenceBit = 0;
                Conn->RxFrame.PayloadSize = 0;

                if(Byte == Addr_Tx || Byte == Addr_Rx) {
                    Conn->RxState = PState_Addr;
                    Conn->RxFrame.Addr = Byte;
                    Conn->HeaderBcc ^= Byte;
                }
                else {
                    dropFrameWhileParsing(Conn, Byte);
                }
            } break;

            case PState_Addr: {
                Conn->HeaderBcc ^= Byte;

                frame_type Type = Byte & Control_TypeMask;
                switch(Type) {
                    case FrameType_Info: {
                        if(Conn->EndpointType == llEndpoint_Rx) {
                            Conn->RxFrame.Type = FrameType_Info;
                            Conn->RxFrame.SequenceBit = (Byte & Control_SeqSMask) >> Control_SeqSShift;

                            Conn->RxState = PState_Control;
                        }
                        else {
                            // NOTE(nox): A transmitter shouldn't receive any Info frame
                            dropFrameWhileParsing(Conn, Byte);
                        }
                    } break;

                    case FrameType_Set: case FrameType_Disc: case FrameType_UAck:
                    case FrameType_RxRdy: case FrameType_Rej:
                    {
                        if(Conn->EndpointType == llEndpoint_Tx ||
                           (Type != FrameType_RxRdy && Type != FrameType_Rej))
                        {
                            Conn->RxFrame.Type = Type;

                            if(Conn->RxFrame.Type == FrameType_RxRdy || Conn->RxFrame.Type == FrameType_Rej) {
                                Conn->RxFrame.SequenceBit = (Byte & Control_SeqRMask) >> Control_SeqRShift;
                            }

                            Conn->RxState = PState_Control;
                        }
                        else {
                            // NOTE(nox): The receiver shouldn't receive any RxRdy or Rej frames
                            dropFrameWhileParsing(Conn, Byte);
                        }
                    } break;

                    default: {
                        dropFrameWhileParsing(Conn, Byte);
                    } break;
                }
            } break;

            case PState_Control: {
                if(Byte == Conn->HeaderBcc) {
                    if(Conn->RxFrame.Type == FrameType_Info) {
                        if(Conn->SequenceBit == Conn->RxFrame.SequenceBit) {
                            // NOTE(nox): New info frame
                            Conn->RxState = PState_ReceivePayload;
                        }
                        else {
                            // NOTE(nox): This info frame is a duplicate; in either case, we'll confirm
                            // it with a RxRdy
                            rejectInfoFrame(Conn, true);
                        }
                    }
                    else {
                        Conn->RxState = PState_OtherBcc;
                    }
                }
                else {
                    dropFrameWhileParsing(Conn, Byte);
                }
            } break;

            case PState_OtherBcc: {
                if(Byte == DelimiterFlag) {
                    finalizeRxFrame(Conn);
                }
                else {
                    dropFrameWhileParsing(Conn, Byte);
                }
            } break;

            case PState_ReceivePayload: {
                if(Byte == DelimiterFlag) {
                    // NOTE(nox): As the last byte is the BCC, it will XOR with itself and, if there are
                    // no errors, PayloadBcc will be 0
                    if(Conn->RxFrame.PayloadSize && Conn->PayloadBcc == 0) {
                        --Conn->RxFrame.PayloadSize;
                        finalizeRxFrame(Conn);
                    }
                    else {
                        rejectInfoFrame(Conn, false);
                    }
                }
                else if(Conn->RxFrame.PayloadSize >= arrayCount(Conn->RxFrame.Payload)) {
                    // NOTE(nox): Payload too big!
                    rejectInfoFrame(Conn, false);
                }
                else if(Byte == EscapeByte) {
                    Conn->RxState = PState_ReceivePayloadEscape;
                }
                else {
                    Conn->RxFrame.Payload[Conn->RxFrame.PayloadSize++] = Byte;
                    Conn->PayloadBcc ^= Byte;
                }
            } break;

            case PState_ReceivePayloadEscape: {
                u8 DestuffedByte;

                if((DestuffedByte = DelimiterFlag, Byte == (DestuffedByte ^ EscapeXor)) ||
                   (DestuffedByte = EscapeByte,    Byte == (DestuffedByte ^ EscapeXor)))
                {
                    Conn->RxFrame.Payload[Conn->RxFrame.PayloadSize++] = DestuffedByte;
                    Conn->PayloadBcc ^= DestuffedByte;

                    Conn->RxState = PState_ReceivePayload;
                }
                else {
                    // NOTE(nox): Invalid escape sequence
                    rejectInfoFrame(Conn, false);
                }
            } break;

            invalidDefaultCase();
        }
    }
}

static inline milli getMilli() {
    milli Result;

    struct timespec TimeSpec;
    clock_gettime(CLOCK_MONOTONIC, &TimeSpec);

    Result = TimeSpec.tv_sec*1000 + TimeSpec.tv_nsec/1000000;

    return Result;
}

static inline milli getElapsed(milli Start) {
    milli Result = getMilli() - Start;

    return Result;
}

static int waitForFrame(ll_conn *Conn, milli Timeout) {
    int Result = 0;

    Conn->RxFrame.Valid = false;

    for(milli StartTime = getMilli();;) {
        int BytesReceived = receiveBytes(Conn);
        if(BytesReceived < 0) {
            Result = -1;
            break;
        }

        tryParseFrame(Conn);

        if(!connRxUnfilled(Conn)) {
            // NOTE(nox): As the RX buffer is filled, it means no frame was parsed and that we can't
            // receive any more data; we'll have to drop this one.
            dropFrame(Conn);
        }
        else if(Conn->RxFrame.Valid || (Timeout >= 0 && getElapsed(StartTime) >= Timeout)) {
            break;
        }
    }

    return Result;
}

static inline bool isFrameType(parsed_frame *Frame, frame_type Type) {
    bool Result = (Frame->Valid && Frame->Type == Type);

    return Result;
}

static inline bool isRxType(ll_conn *Conn, parsed_frame *Frame, rx_type RxType, frame_type FrameType) {
    bool Result = (isFrameType(Frame, FrameType) &&
                   ((RxType == Rx_Command  && Frame->Addr == (Conn->EndpointType == llEndpoint_Rx ? Addr_Tx : Addr_Rx)) ||
                    (RxType == Rx_Response && Frame->Addr == (Conn->EndpointType == llEndpoint_Rx ? Addr_Rx : Addr_Tx)) ||
                    RxType == Rx_Any));

    return Result;
}

enum {
    Wait_Err      = -1,
    Wait_Timeout  = -2,
    Wait_RxedDisc = -3,
    Wait_Rej      = -4,
};
static int waitForType(ll_conn *Conn, rx_type RxType, frame_type RxFrameType, milli Timeout)
{
    int Result = 0;

    milli StartTime = getMilli();
    for(bool First = true;; First = false) {
        milli RemainingTime;
        if(Timeout > 0) {
            RemainingTime = Timeout - getElapsed(StartTime);

            if(RemainingTime < 0) {
                Result = Wait_Timeout;
                break;
            }
        }
        else {
            RemainingTime = Timeout;

            if(Timeout == 0 && !First) {
                Result = Wait_Timeout;
                break;
            }
        }

        if(waitForFrame(Conn, RemainingTime) < 0) {
            Result = Wait_Err;
            break;
        }

        bool IsWanted = isRxType(Conn, &Conn->RxFrame, RxType, RxFrameType);

        if(!IsWanted && isFrameType(&Conn->RxFrame, FrameType_Disc)) {
            Result = Wait_RxedDisc;
            break;
        }
        else if(RxFrameType == FrameType_RxRdy) {
            if(IsWanted) {
                if(Conn->SequenceBit != Conn->RxFrame.SequenceBit) {
                    // NOTE(nox): Sent successfully!
                    debugPrint("Successfully sent message\n");
                    break;
                }
                else {
                    // NOTE(nox): Ignoring repeated acknowledge
                    debugPrint("Ignoring repeated RxRdy\n");
                }
            }
            else if(isRxType(Conn, &Conn->RxFrame, RxType, FrameType_Rej)) {
                if(Conn->SequenceBit == Conn->RxFrame.SequenceBit) {
                    // NOTE(nox): There was an error in the transmission
                    debugPrint("Sent message rejected\n");

                    Result = Wait_Rej;
                    break;
                }
                else {
                    // NOTE(nox): Ignoring old reject
                    debugPrint("Ignoring old Rej\n");
                }
            }
        }
        else if(IsWanted) {
            break;
        }
    }

    return Result;
}

static int sendAndRetry(ll_conn *Conn, frame *FrameToSend, rx_type RxType, frame_type RxFrameType,
                        milli Timeout, u32 TimesToRetry)
{
    int Result = 0;

    bool First = true;

    for(u32 Time = 0;; ++Time, First = false) {
        if(Time >= TimesToRetry) {
            Result = Wait_Timeout;
            break;
        }

        if(!First) {
            debugPrint("Retrying to send\n");
        }

        if(sendFrame(Conn, FrameToSend) < 0) {
            Result = Wait_Err;
            break;
        }

        int WaitResult = waitForType(Conn, RxType, RxFrameType, Timeout);
        if(WaitResult == 0) {
            break;
        }
        else if(WaitResult == Wait_Rej) {
            // NOTE(nox): Received response from receiver, reset retry count
            Time = -1;
        }
        else if(WaitResult != Wait_Timeout) {
            Result = WaitResult;
            break;
        }
    }

    return Result;
}

static void handleDisconnect(ll_conn *Conn) {
    frame Frame;
    fillOtherFrame(Conn, &Frame, FrameType_Disc, true);
    sendAndRetry(Conn, &Frame, Rx_Response, FrameType_UAck, DefaultTimeout, RetryTimes);

    Conn->Established = false;
}

// ------------------------------------------------------------------------------------------
// Public interface
ll_conn *llopen(char *SerialPort, ll_endpoint_type Type) {
#if ErrorTest
    srand48(time(0));
#endif

    ll_conn *Conn = calloc(1, sizeof(ll_conn));
    if(!Conn) {
        goto ErrConnAlloc;
    }

    Conn->SerialFileDesc = open(SerialPort, O_RDWR);
    if(Conn->SerialFileDesc < 0) {
        goto ErrSerialOpen;
    }

    if(setupSerialPort(Conn) < 0) {
        goto ErrSetupSerial;
    }

    if(Type == llEndpoint_Tx) {
        Conn->EndpointType = llEndpoint_Tx;

        frame SetFrame;
        fillOtherFrame(Conn, &SetFrame, FrameType_Set, true);

        if(sendAndRetry(Conn, &SetFrame, Rx_Response, FrameType_UAck, DefaultTimeout, RetryTimes) < 0)
        {
            frame DiscFrame;
            fillOtherFrame(Conn, &DiscFrame, FrameType_Disc, true);
            sendFrame(Conn, &DiscFrame);

            goto ErrComm;
        }
    }
    else {
        Conn->EndpointType = llEndpoint_Rx;

        if(waitForType(Conn, Rx_Command, FrameType_Set, -1) < 0) {
            goto ErrComm;
        }

        frame UAckFrame;
        fillOtherFrame(Conn, &UAckFrame, FrameType_UAck, false);
        if(sendFrame(Conn, &UAckFrame) < 0) {
            goto ErrComm;
        }
    }

    Conn->Established = true;

    debugPrint("Initialized connection!\n");

    return Conn;

  ErrComm:
  ErrSetupSerial:
    close(Conn->SerialFileDesc);
  ErrSerialOpen:
    free(Conn);
  ErrConnAlloc:
    return 0;
}

int llwrite(ll_conn *Conn, u8 *Buf, u32 Size) {
    int Result = 0;

    if(Conn->Established && Conn->EndpointType == llEndpoint_Tx && Size <= llMaximumPayloadSize) {
        frame InfoFrame;
        fillInfoFrame(Conn, &InfoFrame, Buf, Size);

        int SendResult = sendAndRetry(Conn, &InfoFrame, Rx_Response, FrameType_RxRdy, DefaultTimeout, RetryTimes);
        switch(SendResult) {
            case Wait_RxedDisc: {
                Result = llwrite_Disc;
                handleDisconnect(Conn);
            } break;

            case Wait_Timeout: {
                Result = llwrite_Timeout;
            } break;

            case Wait_Err: {
                Result = llwrite_Err;
            } break;

            case 0: {
                Conn->SequenceBit ^= 1;
            } break;

            invalidDefaultCase();
        }
    }
    else {
        Result = llwrite_Err;
    }

    return Result;
}

int llread(ll_conn *Conn, u8 *Buf) {
    int Result = 0;

    if(Conn->Established && Conn->EndpointType == llEndpoint_Rx) {
        int WaitResult = waitForType(Conn, Rx_Command, FrameType_Info, -1);

        if(WaitResult == Wait_Err) {
            Result = llread_Err;
        }
        else if(WaitResult == Wait_RxedDisc) {
            Result = llread_Disc;
            handleDisconnect(Conn);
        }
        else {
            assert(WaitResult == 0);

            assert(Conn->RxFrame.Valid);
            assert(Conn->SequenceBit == Conn->RxFrame.SequenceBit);
            Conn->SequenceBit ^= 1;

            frame RdyFrame;
            fillOtherFrame(Conn, &RdyFrame, FrameType_RxRdy, false);
            sendFrame(Conn, &RdyFrame);

            assert(Conn->RxFrame.PayloadSize <= llMaximumPayloadSize);
            memcpy(Buf, Conn->RxFrame.Payload, Conn->RxFrame.PayloadSize);

            Result = Conn->RxFrame.PayloadSize;
        }
    }
    else {
        Result = llread_Err;
    }

    return Result;
}

int llclose(ll_conn *Conn) {
    int Result = 0;

    if(Conn->Established) {
        int SendResult;

        frame Frame;
        fillOtherFrame(Conn, &Frame, FrameType_Disc, true);

        SendResult = sendAndRetry(Conn, &Frame, Rx_Command, FrameType_Disc, DefaultTimeout, RetryTimes);
        if(SendResult < 0) {
            assert(SendResult != Wait_RxedDisc);
            Result = -1;
            goto Finalize;
        }

        fillOtherFrame(Conn, &Frame, FrameType_UAck, false);
        sendFrame(Conn, &Frame);
    }

  Finalize:
#if ErrorTest
    if(Conn->EndpointType == llEndpoint_Rx) {
        debugPrint("%lu received\n    %lu succeeded | %lu headers failed | %lu payload failed → %.3le | %.3le | %.3le\n",
                   Conn->TotalCount, Conn->SuccessfulCount, Conn->FailedHeaderCount, Conn->FailedPayloadCount,
                   (double)Conn->SuccessfulCount/Conn->TotalCount,
                   (double)Conn->FailedHeaderCount/Conn->TotalCount,
                   (double)Conn->FailedPayloadCount/Conn->TotalCount);
    }
#endif

    restoreSerialPort(Conn);
    close(Conn->SerialFileDesc);
    free(Conn);

    return Result;
}
