#!/usr/bin/env bash
cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1
SrcDir="$(pwd)/.."
RootDir="$SrcDir/.."

. "$SrcDir/setup.sh"

echo Building protocol
gcc $Flags protocol.c -c -o "$BuildDir/protocol.o"
