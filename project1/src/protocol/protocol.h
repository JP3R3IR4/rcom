#if !defined(PROTOCOL_H)
#define PROTOCOL_H

#include <util.h>

enum {
    llMaximumPayloadSize = 200,
};

typedef enum {
    llEndpoint_Rx,
    llEndpoint_Tx,
} ll_endpoint_type;

typedef struct ll_conn ll_conn;

enum {
    llread_Disc = -1,
    llread_Err  = -2,
};

enum {
    llwrite_Disc    = -1,
    llwrite_Timeout = -2,
    llwrite_Err     = -3,
};

// ------------------------------------------------------------------------------------------
// Public function interface
ll_conn *llopen(char *Serialport, ll_endpoint_type Type);

int llwrite(ll_conn *Conn, u8 *Buf, u32 Size);

// NOTE(nox): Buf must have at least llMaximumPayloadSize bytes allocated
int llread(ll_conn *Conn, u8 *Buf);

int llclose(ll_conn *Conn);

#endif // PROTOCOL_H
