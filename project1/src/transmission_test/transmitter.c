#include <util.h>
#include <protocol.h>

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

enum {
    NumFrames = 4,
    NumBytes  = 64,
};

int main(int ArgCount, char *ArgVals[]) {
    if(ArgCount < 2) {
        fprintf(stderr, "Usage: %s SerialPort\n", ArgVals[0]);
        exit(1);
    }

    srand(time(0));

    ll_conn *Conn = llopen(ArgVals[1], llEndpoint_Tx);
    if(!Conn) {
        fprintf(stderr, "Could not initialize connection\n");
        exit(1);
    }

    u8 Buf[NumBytes];

    for(int FrameIdx = 0; FrameIdx < NumFrames; ++FrameIdx) {
        for(int Idx = 0; Idx < NumBytes; ++Idx) {
            Buf[Idx] = (u8)rand();
        }

        printf("Sending %d bytes:\n", NumBytes);
        for(int Idx = 0; Idx < NumBytes; ++Idx) {
            if(Idx && (Idx % 16) == 0) {
                puts("");
            }

            printf("%02X ", Buf[Idx]);
        }
        puts("");

        int WriteResult = llwrite(Conn, Buf, NumBytes);
        if(WriteResult == llwrite_Disc) {
            puts("Disconnected.");
            break;
        }
        else if(WriteResult < 0) {
            fprintf(stderr, "Error transmitting (%d)\n", WriteResult);
            break;
        }
        else {
            puts("Success!");
        }
    }

    llclose(Conn);

    return 0;
}
