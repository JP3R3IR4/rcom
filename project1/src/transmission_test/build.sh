#!/usr/bin/env bash
cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1
SrcDir="$(pwd)/.."
RootDir="$SrcDir/.."

. "$SrcDir/setup.sh"

Flags="$Flags -I$SrcDir/protocol"

echo Building transmission test
gcc $Flags "$(pwd)/transmitter.c" "$BuildDir/protocol.o" -o "$BuildDir/test_transmitter.out"
gcc $Flags "$(pwd)/receiver.c"    "$BuildDir/protocol.o" -o "$BuildDir/test_receiver.out"
