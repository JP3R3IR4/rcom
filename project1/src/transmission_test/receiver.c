#include <util.h>
#include <protocol.h>

#include <stdio.h>
#include <stdlib.h>

int main(int ArgCount, char *ArgVals[]) {
    if(ArgCount < 2) {
        fprintf(stderr, "Usage: %s SerialPort\n", ArgVals[0]);
        exit(1);
    }

    ll_conn *Conn = llopen(ArgVals[1], llEndpoint_Rx);
    if(!Conn) {
        fprintf(stderr, "Could not initialize connection\n");
        exit(1);
    }

    u8 Buf[llMaximumPayloadSize];
    for(;;) {
        int ReadResult = llread(Conn, Buf);
        if(ReadResult < 0) {
            if(ReadResult != llread_Disc) {
                fprintf(stderr, "Error receiving (%d)\n", ReadResult);
            }
            break;
        }

        printf("Received %d bytes:\n", ReadResult);
        for(int Idx = 0; Idx < ReadResult; ++Idx) {
            if(Idx && (Idx % 16) == 0) {
                puts("");
            }

            printf("%02X ", Buf[Idx]);
        }
        puts("");
        puts("Success!");
    }

    llclose(Conn);

    return 0;
}
