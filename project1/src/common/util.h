#if !defined(UTIL_H)
#define UTIL_H

#include <stdint.h>
#include <stdbool.h>

#define arrayCount(Arr) (sizeof(Arr)/sizeof(*(Arr)))

#define invalidCodePath() assert(!"Invalid code path")
#define invalidCase(Case) case Case: { assert(!"Invalid case"); } break
#define invalidDefaultCase() default: { assert(!"Invalid default case"); } break

#define staticAssert(X) typedef int __static_assert_##__FILE__##__LINE__[(X) ? 1 : -1]

typedef int8_t  s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

static int min(int A, int B) {
    return A < B ? A : B;
}

#endif // UTIL_H
