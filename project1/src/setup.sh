BuildDir="$RootDir/build"
mkdir -p "$BuildDir"

CommonFlags="-Wall -Wextra -Wno-unused-function -I$SrcDir/common"
DebugFlags="-ggdb"
ReleaseFlags="-O2"

Flags="$CommonFlags $DebugFlags"
