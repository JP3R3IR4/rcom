#!/usr/bin/env bash
cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1

protocol/build.sh
application/build.sh
transmission_test/build.sh
