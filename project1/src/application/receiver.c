#include <util.h>
#include <protocol.h>
#include "app_protocol.h"
#include "crc32.c"

#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

typedef enum {
    ParsedPacket_Invalid,
    ParsedPacket_Control,
    ParsedPacket_Data,
} parsed_packet_type;

typedef struct {
    parsed_packet_type Type;

    union {
        struct {
            bool Start;

            u64 FileSize;

            u8 FileNameLength;
            char *FileName;

            u32 Crc32;
        };

        struct {
            u64 DataLength;
            u8  SeqNum;
            u8 *Data;
        };
    };
} parsed_packet;

static void parsePacket(u8 *Buf, u64 BytesRead, parsed_packet *Packet) {
    assert(BytesRead > 0);
    *Packet = (parsed_packet){};

    u64 Idx = 0;
    switch(Buf[Idx++]) {
        {
            while(0) { case PacketType_ControlStart: Packet->Start = true;  }
            while(0) { case PacketType_ControlEnd:   Packet->Start = false; }

            Packet->Type = ParsedPacket_Control;

            while(Idx + 2 <= BytesRead) {
                u8 TypeByte = Buf[Idx++];
                u8 LenByte  = Buf[Idx++];
                u64 AfterLastByte = Idx + LenByte;

                if(AfterLastByte <= BytesRead) {
                    switch(TypeByte) {
                        case ControlPacket_FileSize: {
                            while(Idx < AfterLastByte) {
                                Packet->FileSize = Packet->FileSize*10 + Buf[Idx++] - '0';
                            }
                        } break;

                        case ControlPacket_FileName: {
                            Packet->FileNameLength = LenByte;
                            Packet->FileName       = (char *)(Buf + Idx);
                            Idx += LenByte;

                            for(int NameIdx = 0; NameIdx < LenByte; ++NameIdx) {
                                if(Packet->FileName[NameIdx] == '/') {
                                    fprintf(stderr, "Received start control packet with a slash in the name.\n");
                                    Packet->FileNameLength = 0;
                                    Packet->FileName       = 0;
                                    break;
                                }
                            }
                        } break;

                        case ControlPacket_Crc32: {
                            if(LenByte == sizeof(Packet->Crc32)) {
                                Packet->Crc32 = *(u32 *)(Buf + Idx);
                            }

                            Idx += LenByte;
                        } break;

                        default: {
                            Idx += LenByte;
                        } break;
                    }
                }
                else {
                    break;
                }
            }
        } break;

        case PacketType_Data: {
            if(Idx + 3 <= BytesRead) {
                Packet->SeqNum = Buf[Idx++];
                Packet->DataLength = (Buf[Idx] << 8) | Buf[Idx + 1];
                Idx += 2;

                if(Idx + Packet->DataLength <= BytesRead) {
                    Packet->Type = ParsedPacket_Data;
                    Packet->Data = Buf + Idx;
                }
            }
        };
    }
}

static u32 getTerminalWidth(u32 FileDesc) {
	u32 Width = 80;

	if(isatty(FileDesc)) {
        struct winsize WindowSize;
        if(ioctl(FileDesc, TIOCGWINSZ, &WindowSize) == 0 && WindowSize.ws_col > 0) {
            Width = WindowSize.ws_col;
        }
    }

    return Width;
}

void printProgressBar(u64 FileSize, u64 BytesWritten) {
    u32 Width = getTerminalWidth(STDOUT_FILENO);
    u32 ProgressBarWidth = Width - 4 - 1 - 6 - 3;
    u32 BytesPerChar = FileSize/ProgressBarWidth;
    u32 BytesToFill  = BytesWritten/BytesPerChar;

    float Percentage = (float)BytesWritten*100/FileSize;

    fputs("   [", stdout);
    for(u32 Idx = 0; Idx < ProgressBarWidth; ++Idx) {
        if(Idx < BytesToFill) {
            putc('#', stdout);
        }
        else {
            putc(' ', stdout);
        }
    }
    printf("] %5.1f%%\r", Percentage);
    fflush(stdout);
}

int main(int ArgCount, char *ArgVals[]) {
    if(ArgCount < 2) {
        fprintf(stderr, "Usage: %s SerialPort [TargetFolder]\n", ArgVals[0]);
        exit(1);
    }

    char *TargetFolderPath = 0;
    if(ArgCount >= 3) {
        TargetFolderPath = ArgVals[2];
    }

    ll_conn *Conn = llopen(ArgVals[1], llEndpoint_Rx);
    if(!Conn) {
        fprintf(stderr, "Could not initialize connection\n");
        exit(1);
    }

    u8 Buf[llMaximumPayloadSize];
    bool WaitingForStart = true;
    int TargetFileDesc = -1;
    u64 FileSize = 0;
    u64 BytesWritten = 0;
    u64 SeqNum = 0;
    u32 Crc32  = 0;

    for(;;) {
        int BytesRead = llread(Conn, Buf);
        if(BytesRead < 0) {
            if(BytesRead != llread_Disc) {
                fprintf(stderr, "Error receiving (%d)\n", BytesRead);
            }
            else if(!WaitingForStart) {
                assert(BytesRead == llread_Disc);
                fprintf(stderr, "Disconnected before receiving entire file!\n");
            }

            break;
        }

        parsed_packet Packet;
        parsePacket(Buf, BytesRead, &Packet);
        if(WaitingForStart) {
            if(Packet.Type == ParsedPacket_Control && Packet.Start == true &&
               Packet.FileSize && Packet.FileName && Packet.FileNameLength)
            {
                WaitingForStart = false;
                BytesWritten = 0;
                SeqNum = 0;

                char *FilePath;
                char *WriteLocation;
                if(TargetFolderPath) {
                    int FolderLen = strlen(TargetFolderPath);
                    FilePath = malloc((FolderLen + 1 + Packet.FileNameLength + 1)*sizeof(*FilePath));
                    strcpy(FilePath, TargetFolderPath);
                    FilePath[FolderLen] = '/';

                    WriteLocation = FilePath + FolderLen + 1;
                }
                else {
                    WriteLocation = FilePath = malloc((Packet.FileNameLength + 1)*sizeof(*FilePath));
                }

                strncpy(WriteLocation, Packet.FileName, Packet.FileNameLength);
                WriteLocation[Packet.FileNameLength] = 0;

                TargetFileDesc = open(FilePath, O_RDWR|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
                if(TargetFileDesc < 0) {
                    fprintf(stderr, "Could not open target file %s\n", FilePath);
                    goto FileOpenErr;
                }

                FileSize = Packet.FileSize;
                if(posix_fallocate(TargetFileDesc, 0, FileSize) != 0) {
                    fprintf(stderr, "Could not allocate %lu bytes for file %s\n", FileSize, FilePath);
                    goto FileAllocErr;
                }

                while(0) {
                  FileAllocErr:
                    close(TargetFileDesc);
                  FileOpenErr:
                    WaitingForStart = true;
                }

                free(FilePath);

                Crc32 = Packet.Crc32;
            }
        }
        else {
            if(Packet.Type == ParsedPacket_Control && Packet.Start == false) {
                puts("");

                u8 *FileContents = mmap(0, FileSize, PROT_READ, MAP_SHARED, TargetFileDesc, 0);
                if(FileContents) {
                    u32 TestCrc32 = calculateCrc32(FileContents, FileSize);
                    munmap(FileContents, FileSize);

                    if(TestCrc32 != Crc32) {
                        fprintf(stderr, "CRC32 doesn't match:\n  Transmitted: %08x\n   Calculated: %08x\n",
                                Crc32, TestCrc32);
                    }
                    else {
                        puts("Transmission \033[32mcomplete\033[0m!");
                    }
                }
                else {
                    fprintf(stderr, "Could not calculate CRC32\n");
                }

                close(TargetFileDesc);
                WaitingForStart = true;
            }
            else if(Packet.Type == ParsedPacket_Data) {
                if(BytesWritten < FileSize) {
                    if((SeqNum & 0xFF) != Packet.SeqNum) {
                        fprintf(stderr, "Wrong sequence number: expected %u, got %u\n",
                                (u8)(SeqNum & 0xFF), Packet.SeqNum);

                        SeqNum = Packet.SeqNum;
                    }

                    int NewBytesWritten = write(TargetFileDesc, Packet.Data,
                                                min(Packet.DataLength, FileSize-BytesWritten));
                    if(NewBytesWritten < 0) {
                        fprintf(stderr, "Error writing to file!\n");
                        close(TargetFileDesc);
                        goto TerminateConn;
                    }

                    BytesWritten += NewBytesWritten;
                    SeqNum       += NewBytesWritten;
                }
                else {
                    fprintf(stderr, "Ignoring data packets because file size has already been reached (%lu bytes)\n",
                            FileSize);
                }

                printProgressBar(FileSize, BytesWritten);
            }
        }
    }

  TerminateConn:
    llclose(Conn);

    return 0;
}
