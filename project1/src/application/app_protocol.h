#if !defined(APP_PROTOCOL_H)
#define APP_PROTOCOL_H

enum {
    PacketType_Data         = 0x01,
    PacketType_ControlStart = 0x02,
    PacketType_ControlEnd   = 0x03,

    ControlPacket_FileSize = 0,
    ControlPacket_FileName,
    ControlPacket_Crc32,
};

#endif // APP_PROTOCOL_H
