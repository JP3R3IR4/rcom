#include <util.h>
#include <protocol.h>
#include "app_protocol.h"
#include "crc32.c"

#include <assert.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

enum {
    ChunkSize = llMaximumPayloadSize-4,
};
staticAssert((u64)ChunkSize+4 <= (u64)llMaximumPayloadSize);

typedef struct {
    u64 Size;
    u8 Buf[llMaximumPayloadSize];
} packet;

static char *getFileNameComponent(char *FileName) {
    u64 Offset = 0;
    int Length = strlen(FileName);
    for(int Idx = 0; Idx < Length; ++Idx) {
        if(FileName[Idx] == '/') {
            Offset = Idx+1;
        }
    }

    return FileName + Offset;
}

static bool fillControlPacket(packet *Packet, bool Start, u64 FileSize, char *FilePath, u32 Crc32) {
    bool Result = true;

    *Packet = (packet){};

    u8 *Buf = Packet->Buf;
    Buf[Packet->Size++] = Start ? PacketType_ControlStart : PacketType_ControlEnd;

    // NOTE(nox): Encode file size
    u8 AsciiFileSizeLength = 0;
    char AsciiFileSize[20];
    do {
        assert(AsciiFileSizeLength < arrayCount(AsciiFileSize));
        AsciiFileSize[AsciiFileSizeLength++] = FileSize % 10 + '0';
    } while(FileSize /= 10);

    Buf[Packet->Size++] = ControlPacket_FileSize;
    Buf[Packet->Size++] = AsciiFileSizeLength;
    for(int Idx = AsciiFileSizeLength-1; Idx >= 0; --Idx) {
        Buf[Packet->Size++] = AsciiFileSize[Idx];
    }

    // NOTE(nox): Encode CRC32
    Buf[Packet->Size++] = ControlPacket_Crc32;
    Buf[Packet->Size++] = sizeof(Crc32);
    *(u32 *)(Buf + Packet->Size) = Crc32;
    Packet->Size += sizeof(Crc32);

    // NOTE(nox): Encode file name
    char *FileName = getFileNameComponent(FilePath);
    s32 MaxFileName = llMaximumPayloadSize - Packet->Size - 2;
    if(MaxFileName <= 0) {
        fprintf(stderr, "No available space in the control packet for any character of the file name!\n");
        Result = false;
    }
    else {
        u8 FileNameLength = min(strlen(FileName), MaxFileName);

        Buf[Packet->Size++] = ControlPacket_FileName;
        Buf[Packet->Size++] = FileNameLength;
        for(int Idx = 0; Idx < FileNameLength; ++Idx) {
            Buf[Packet->Size++] = FileName[Idx];
        }
    }

    assert(Packet->Size <= llMaximumPayloadSize);

    return Result;
}

static void fillDataPacket(packet *Packet, u8 *Data, u64 Size, u64 SeqNum) {
    *Packet = (packet){};

    assert(4 + Size <= arrayCount(Packet->Buf));

    u8 *Buf = Packet->Buf;
    Buf[Packet->Size++] = PacketType_Data;
    Buf[Packet->Size++] = SeqNum & 0xFF;

    Buf[Packet->Size++] = (Size >> 8) & 0xFF;
    Buf[Packet->Size++] = (Size >> 0) & 0xFF;

    for(u64 Idx = 0; Idx < Size; ++Idx) {
        Buf[Packet->Size++] = Data[Idx];
    }
}

int main(int ArgCount, char *ArgVals[]) {
    if(ArgCount < 3) {
        fprintf(stderr, "Usage: %s SerialPort File\n", ArgVals[0]);
        exit(1);
    }

    char *FilePath = ArgVals[2];
    int FileDesc = open(FilePath, O_RDONLY);
    if(FileDesc < 0) {
        fprintf(stderr, "Invalid or inexistent file passed in: %s\n", FilePath);
        exit(1);
    }

    struct stat FileStat;
    if(fstat(FileDesc, &FileStat) < 0 || !S_ISREG(FileStat.st_mode)) {
        fprintf(stderr, "Error running stat on file %s\n", FilePath);
        exit(1);
    }

    u64 FileSize = (u64)FileStat.st_size;
    u8 *FileContents = mmap(0, FileSize, PROT_READ, MAP_SHARED, FileDesc, 0);
    u32 Crc32 = calculateCrc32(FileContents, FileSize);

    ll_conn *Conn = llopen(ArgVals[1], llEndpoint_Tx);
    if(!Conn) {
        fprintf(stderr, "Could not initialize connection\n");
        exit(1);
    }

    packet Packet;
    fillControlPacket(&Packet, true, FileSize, FilePath, Crc32);
    int WriteResult = llwrite(Conn, Packet.Buf, Packet.Size);
    if(WriteResult < 0) {
        fprintf(stderr, "Error sending control start packet (%d)\n", WriteResult);
        goto WriteErr;
    }

    for(u64 SeqNum = 0, Offset = 0; Offset < FileSize; Offset += ChunkSize) {
        u64 ByteCountToSend = Offset + ChunkSize > FileSize ? FileSize - Offset : ChunkSize;
        fillDataPacket(&Packet, FileContents + Offset, ByteCountToSend, SeqNum);
        WriteResult = llwrite(Conn, Packet.Buf, Packet.Size);
        if(WriteResult < 0) {
            fprintf(stderr, "Error sending data packet (%d)\n", WriteResult);
            goto WriteErr;
        }

        SeqNum += ByteCountToSend;
    }

    fillControlPacket(&Packet, false, FileSize, FilePath, Crc32);
    WriteResult = llwrite(Conn, Packet.Buf, Packet.Size);
    if(WriteResult < 0) {
        fprintf(stderr, "Error sending control end packet (%d)\n", WriteResult);
        goto WriteErr;
    }

  WriteErr:
    llclose(Conn);
    munmap(FileContents, FileSize);
    close(FileDesc);

    return 0;
}
