#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>

#define arrayCount(Arr) (sizeof(Arr)/sizeof(*(Arr)))

int main(int ArgCount, char *ArgVals[]) {
    if(ArgCount < 2 ||
       (strcmp("/dev/ttyS0", ArgVals[1]) != 0 &&
        strcmp("/dev/ttyS1", ArgVals[1]) != 0))
    {
        fprintf(stderr, "Usage: %s SerialPort\n", ArgVals[0]);
        exit(1);
    }

    int SerialFileDesc = open(ArgVals[1], O_RDWR | O_NOCTTY);
    if(SerialFileDesc < 0) {
        fprintf(stderr, "Could not open serial port %s\n", ArgVals[1]);
        exit(1);
    }

    struct termios SavedTerminalConfig;
    if(tcgetattr(SerialFileDesc, &SavedTerminalConfig) == -1) {
        fprintf(stderr, "Could not get the serial port parameters\n");
        exit(1);
    }

    struct termios TerminalConfig = {};
    TerminalConfig.c_cflag = B38400 | CS8 | CLOCAL | CREAD;
    TerminalConfig.c_iflag = IGNPAR;
    TerminalConfig.c_oflag = 0;
    TerminalConfig.c_lflag = 0;

    TerminalConfig.c_cc[VTIME] = 1;
    TerminalConfig.c_cc[VMIN]  = 0;

    tcflush(SerialFileDesc, TCIOFLUSH);
    if(tcsetattr(SerialFileDesc, TCSANOW, &TerminalConfig) == -1) {
        fprintf(stderr, "Could not set the serial port parameters\n");
        exit(-1);
    }

    char Buffer[1<<10];
    printf("Line to send: ");
    if(!fgets(Buffer, arrayCount(Buffer), stdin)) {
        fprintf(stderr, "No text to send :(\n");
        exit(1);
    }

    int StringLength = strlen(Buffer);
    int CharactersSent = write(SerialFileDesc, Buffer, StringLength+1);
    printf("Sent %d bytes\n", CharactersSent);

    int TotalBytesRead = 0;
    while(TotalBytesRead < (int)(arrayCount(Buffer)-1)) {
        int CharactersRead = read(SerialFileDesc, Buffer+TotalBytesRead, arrayCount(Buffer)-1-TotalBytesRead);
        if(CharactersRead > 0) {
            TotalBytesRead += CharactersRead;
            if(Buffer[TotalBytesRead-1] == 0) {
                break;
            }
        }
    }
    printf("Read %d bytes: \"%s\"\n", TotalBytesRead, Buffer);

    tcflush(SerialFileDesc, TCIOFLUSH);
    if(tcsetattr(SerialFileDesc, TCSANOW, &SavedTerminalConfig) == -1) {
        fprintf(stderr, "Could not reset to the saved serial port parameters\n");
        exit(-1);
    }
    close(SerialFileDesc);

    return 0;
}
