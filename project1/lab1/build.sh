#!/usr/bin/env bash
ScriptDir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
cd "$ScriptDir"

gcc -ggdb -Wall -Wextra -Wno-unused-function transmitter.c -o transmitter.out
gcc -ggdb -Wall -Wextra -Wno-unused-function receiver.c    -o receiver.out
